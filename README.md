# 框架使用文档

#### 介绍
本开源项目包含一套完整的开发流程,希望能够在工作减少频繁的人工操作,尽可能的通过自动化程序代替人工,能够让开发人员只关注需求本身,开发过程中的一系列重复劳动都可以省略.

#### 需要使用的工具
1.  微服务框架
    1. ELK
    2. Kafka
    3. Nacos
    4. Seata
    5. Sentinel
2.  代码生成工具
3.  Jenkins
4.  阿里云云效
5.  阿里云语雀
6.  SonarQube
7.  Kubernetes/Kuboard
8.  Skywalking
9.  钉钉机器人


#### 安装教程

1.  安装Jenkins
2.  安装SonarQube8.5+
3.  安装Kubernetes/Kuboard
4.  安装Skywalking
5.  安装Kafka集群
6.  安装ELK
7.  安装Nacos/Seata/Sentinel


#### 使用说明

1.  框架使用说明
2.  开发环境集成

