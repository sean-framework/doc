## 安装 Jenkins

### 主机安装

https://www.cnblogs.com/yangzp/p/12953810.html

### Docker安装

https://segon.cn/install-jenkins-using-docker.html

```
使用流水线集成Docker,k8s打包发布建议安装在主机中
```



### 修改端口

https://blog.csdn.net/wudinaniya/article/details/97628477



### 需要安装的插件

1. [DingTalk](https://plugins.jenkins.io/dingding-notifications)
2. [Docker Pipeline](https://plugins.jenkins.io/docker-workflow)
3. [ Generic Webhook Trigger Plugin](https://plugins.jenkins.io/generic-webhook-trigger)
4. [Kubernetes plugin](https://plugins.jenkins.io/kubernetes)
5. [Build Name and Description Setter](https://plugins.jenkins.io/build-name-setter)
6. [Pipeline Utility Steps](https://plugins.jenkins.io/pipeline-utility-steps)



### 添加Git凭证

https://blog.csdn.net/nklinsirui/article/details/89404605



### 添加Maven配置

文件路径:  /var/lib/jenkins/.m2



### 添加Docker配置

文件路径:  /var/lib/jenkins/.docker



### 添加SSH配置

文件路径:  /var/lib/jenkins/.ssh



### 配置Git账号

文件路径: /var/lib/jenkins/.gitconfig 



### 添加钉钉机器人

https://blog.csdn.net/weiqi7777/article/details/108522330

https://jenkinsci.github.io/dingtalk-plugin/ (官方文档)

```
机器人ID: 在系统配置里添加机器人
```



### Pipeline Docker Doc

https://www.jenkins.io/doc/book/pipeline/docker/



## FAQ

### Pipeline中不能使用sudo问题

https://blog.csdn.net/nklinsirui/article/details/80287560

修改后必须重启Jenkins

还是不行,使用下面方法

```
vim /etc/sudoers 增加jenkins(当前用户名）
root ALL=(ALL) ALL
jenkins ALL=(ALL) NOPASSWD:ALL
```

